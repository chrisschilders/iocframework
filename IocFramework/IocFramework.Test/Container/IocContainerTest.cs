﻿using IocFramework.Container;
using IocFramework.Exception;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IocFramework.Test.Container
{
    [TestClass]
    public class IocContainerTest
    {
        [TestMethod]
        [ExpectedException(typeof(TypeNotRegisteredException))]
        public void Should_Throw_TypeNotRegisteredException()
        {
            var container = new IocContainer();
            // container.Register<IConcrete, Concrete>(); -- Intentionally bad

            container.Resolve<IConcrete>();
        }

        [TestMethod]
        public void Should_Create_Instance()
        {
            var container = new IocContainer();
            container.Register<IConcrete, Concrete>();

            var instance = container.Resolve<IConcrete>();
            Assert.IsInstanceOfType(instance, typeof(IConcrete));
        }

        [TestMethod]
        public void Should_Create_InstanceWithParam()
        {
            var container = new IocContainer();
            container.Register<IConcrete, Concrete>();
            container.Register<IConcreteWithParam, ConcreteWithParam>();

            var instance = container.Resolve<IConcreteWithParam>();
            Assert.IsInstanceOfType(instance, typeof(IConcreteWithParam));
        }

        [TestMethod]
        public void Should_Create_InstanceWithParamNested()
        {
            var container = new IocContainer();
            container.Register<IConcrete, Concrete>();
            container.Register<IConcreteWithParam, ConcreteWithParam>();
            container.Register<IConcreteWithParamNested, ConcreteWithParamNested>();

            var instance = container.Resolve<IConcreteWithParamNested>();
            Assert.IsInstanceOfType(instance, typeof(IConcreteWithParamNested));
        }

        [TestMethod]
        public void Should_Create_Singleton_Instance()
        {
            var container = new IocContainer();
            container.Register<IConcrete, Concrete>();
            
            /** 
             * Resolving 2 times with the same object means its a singleton
             */
            var instance = container.Resolve<IConcrete>();
            Assert.AreSame(instance, container.Resolve<IConcrete>()); 
        }

        [TestMethod]
        public void Should_Create_Transient_Instance()
        {
            var container = new IocContainer();
            container.Register<IConcrete, Concrete>(LifeTime.Transient);
            
            /**
             * Resolving 2 times with a different object means its a transient
             */
            var instance = container.Resolve<IConcrete>();
            Assert.AreNotSame(instance, container.Resolve<IConcrete>());
        }
    }

    internal interface IConcrete
    {
    }
    internal class Concrete : IConcrete
    {
    }
    internal interface IConcreteWithParam
    {
    }
    internal class ConcreteWithParam : IConcreteWithParam
    {
        public ConcreteWithParam(IConcrete concrete)
        {

        }
    }
    internal interface IConcreteWithParamNested
    {
    }
    internal class ConcreteWithParamNested : IConcreteWithParamNested
    {
        public ConcreteWithParamNested(IConcreteWithParam concreteWithParams)
        {

        }
    }
}
