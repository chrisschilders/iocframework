﻿using System;
using IocFramework.Container;

namespace Example
{
    /**
     * This would be a typical implementation
     * for an ioc framework configuration
     * 
     *
     */

    /// <summary>
    /// Configuration for IocFramework
    /// </summary>
    public static class IocConfig
    {
        #region IoC Container
        private static readonly Lazy<IContainer>
            LazyContainer = new Lazy<IContainer>(() =>
            {
                var container = new IocContainer();
                RegisterTypes(container);
                return container;
            });

        /// <summary>
        /// Gets the configured ioc container
        /// </summary>
        public static IContainer GetContainer() => 
            LazyContainer.Value;

        #endregion

        /// <summary>
        /// Registers the type mappings with the ioc container
        /// </summary>
        /// <param name="container">Container to configure</param>
        public static void RegisterTypes(IContainer container)
        {
            // container.Register<IConcreteSerivce, ConcreteSerivce>();
        }
    }
}
