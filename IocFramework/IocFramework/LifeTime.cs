namespace IocFramework
{
    /// <summary>
    /// Life time of the instance
    /// </summary>
    public enum LifeTime
    {
        Singleton,
        Transient
    }
}