﻿using System;
using System.Collections.Generic;
using System.Linq;
using IocFramework.Exception;
using IocFramework.Registration;

namespace IocFramework.Container
{
    public class IocContainer : IContainer
    {
        private readonly IList<IRegisteredObject> _registeredObjects;

        public IocContainer()
        {
            _registeredObjects = new List<IRegisteredObject>();
        }

        /// <summary>
        /// Register a type to IoC container
        /// </summary>
        /// <typeparam name="TResolve">Type to resolve</typeparam>
        /// <typeparam name="TConcrete">Type that is used to resolve</typeparam>
        public void Register<TResolve, TConcrete>() where TConcrete : class, TResolve
        {
            Register<TResolve, TConcrete>(LifeTime.Singleton);
        }

        /// <summary>
        /// Register a type to IoC container
        /// </summary>
        /// <param name="instance">Concrete instance</param>
        /// <typeparam name="TResolve">Type requested to resolve</typeparam>
        /// <typeparam name="TConcrete">Type that will be resolved</typeparam>
        public void Register<TResolve, TConcrete>(TConcrete instance) where TConcrete : class, TResolve
        {
            Register<TResolve, TConcrete>(instance, LifeTime.Singleton);
        }

        /// <summary>
        /// Register a type to IoC container
        /// </summary>
        /// <param name="lifeTime">life time of instance</param>
        /// <typeparam name="TResolve">Type requested to resolved</typeparam>
        /// <typeparam name="TConcrete">Type that will be resolved</typeparam>
        public void Register<TResolve, TConcrete>(LifeTime lifeTime) where TConcrete : class, TResolve
        {
            _registeredObjects.Add(new RegisteredObject<TResolve, TConcrete>(lifeTime));
        }

        /// <summary>
        /// Add a registration to IoC container
        /// </summary>
        /// <param name="instance">Concrete instance</param>
        /// <param name="lifeTime">LifeTime of instance</param>
        /// <typeparam name="TResolve">Type requested to resolved</typeparam>
        /// <typeparam name="TConcrete">Type that will be resolved</typeparam>
        public void Register<TResolve, TConcrete>(TConcrete instance, LifeTime lifeTime) where TConcrete : class, TResolve
        {
            _registeredObjects.Add(new RegisteredObject<TResolve, TConcrete>(lifeTime, instance));
        }

        /// <summary>
        /// Resolve an object by registered type
        /// </summary>
        /// <exception cref="TypeNotRegisteredException">If type is not registered in the IocContainer</exception>
        /// <typeparam name="TResolve"></typeparam>
        /// <returns></returns>
        public TResolve Resolve<TResolve>() where TResolve : class
        {
            return ResolveObject(typeof(TResolve)) as TResolve;
        }

        /// <summary>
        /// Resolve an object by registered type
        /// </summary>
        /// <exception cref="TypeNotRegisteredException">If type is not registered in the IocContainer</exception>
        /// <param name="toResolve">Type to resolve</param>
        /// <returns></returns>
        public object Resolve(Type toResolve)
        {
            return ResolveObject(toResolve);
        }

        private object ResolveObject(Type toResolve)
        {
            var registeredObject = _registeredObjects.FirstOrDefault(o => o.ToResolve == toResolve);
            if (registeredObject == null)
                throw new TypeNotRegisteredException($"The type {toResolve.Name} has not been registered");

            return GetInstance(registeredObject);
        }

        private object GetInstance(IRegisteredObject registeredObject)
        {
            if (registeredObject.Instance != null && registeredObject.LifeTime != LifeTime.Transient)
                return registeredObject.Instance;

            var @params = ResolveConstructorParameters(registeredObject).ToArray();

            registeredObject.CreateInstance(@params);
            return registeredObject.Instance;
        }

        private IEnumerable<object> ResolveConstructorParameters(IRegisteredObject registeredObject)
        {
            var ctorInfo = registeredObject.Concrete.GetConstructors().First();

            // Resolve all parameters
            foreach (var parameter in ctorInfo.GetParameters())
            {
                yield return ResolveObject(parameter.ParameterType);
            }
        }
    }
}