﻿using System;

namespace IocFramework.Container
{
    public interface IContainer
    {
        void Register<TResolve, TConcrete>() where TConcrete : class, TResolve;
        void Register<TResolve, TConcrete>(TConcrete instance) where TConcrete : class, TResolve;
        void Register<TResolve, TConcrete>(LifeTime lifeTime) where TConcrete : class, TResolve;
        void Register<TResolve, TConcrete>(TConcrete instance, LifeTime lifeTime)
            where TConcrete : class, TResolve;

        TResolve Resolve<TResolve>() where TResolve : class;
        object Resolve(Type toResolve);
    }
}
