using System;

namespace IocFramework.Exception
{
    [Serializable]
    public class TypeNotRegisteredException : System.Exception
    {
        public TypeNotRegisteredException(string message): base(message) { }
    }
}